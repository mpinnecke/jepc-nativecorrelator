package de.umr.jepc.engine.correlator.demo;

import de.umr.jepc.Attribute;
import de.umr.jepc.EPProvider;
import de.umr.jepc.OutputProcessor;
import de.umr.jepc.engine.NativeEngine;
import de.umr.jepc.engine.correlator.StreamJoinOperator;
import de.umr.jepc.engine.correlator.collections.ArrayListSweepArea;
import de.umr.jepc.engine.correlator.stream.EventObject;
import de.umr.jepc.engine.correlator.stream.RawData;
import de.umr.jepc.engine.correlator.stream.TimeSpan;
import de.umr.jepc.epa.Correlator;
import de.umr.jepc.epa.Stream;
import de.umr.jepc.epa.parameter.booleanexpression.BooleanExpression;
import de.umr.jepc.epa.parameter.predicate.Equal;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Marcus Pinnecke
 */
public class NativeEngineDemo {

    public static void main(String[] args) {

        final String stream1 = "Stream1";
        final String stream2 = "Stream2";
        final Attribute[] schemaStream1 = new Attribute[] {new Attribute("sa", Attribute.DataType.STRING), new Attribute("ia", Attribute.DataType.INTEGER)};
        final Attribute[] schemaStream2 = new Attribute[] {new Attribute("sa", Attribute.DataType.STRING), new Attribute("ia", Attribute.DataType.INTEGER)};
        final String refLeft = "LHS";
        final String refRight = "RHS";
        final BooleanExpression joinCondition = new Equal("RHS.sa", "a");

        EPProvider nativeEngine = new NativeEngine();
        nativeEngine.registerStream(stream1, schemaStream1);
        nativeEngine.registerStream(stream2, schemaStream2);
        nativeEngine.createQuery(new Correlator("Cor", new Stream(stream1), refLeft, new Stream(stream2), refRight, joinCondition));

        List<Object[]> outputNE = new ArrayList<>();

        nativeEngine.addOutputProcessor(new Stream("Cor"), new OutputProcessor() {
            @Override
            public void process(String stream, Object[] event) {
                System.out.println("NE: " + Arrays.toString(event));
                outputNE.add(event);
            }
        });

        nativeEngine.pushEvent("Stream1", new Object[]{"c", 1}, 1, 8);
        nativeEngine.pushEvent("Stream1", new Object[]{"a", 1}, 5, 11);
        nativeEngine.pushEvent("Stream1", new Object[]{"d", 2}, 6, 14);
        nativeEngine.pushEvent("Stream1", new Object[]{"a", 2}, 9, 10);
        nativeEngine.pushEvent("Stream1", new Object[]{"b", 3}, 12, 17);

        nativeEngine.pushEvent("Stream2", new Object[]{"b", 1}, 1, 7);
        nativeEngine.pushEvent("Stream2", new Object[]{"d", 2}, 3, 9);
        nativeEngine.pushEvent("Stream2", new Object[]{"a", 3}, 4, 5);
        nativeEngine.pushEvent("Stream2", new Object[]{"b", 4}, 7, 15);
        nativeEngine.pushEvent("Stream2", new Object[]{"e", 5}, 10, 18);


    }
}
