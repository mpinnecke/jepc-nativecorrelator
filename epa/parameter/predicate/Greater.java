/*
 * Copyright (c) 2012, 2013
 * Database Research Group, University of Marburg.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.umr.jepc.epa.parameter.predicate;

import de.umr.jepc.engine.correlator.utils.Evaluator;
import de.umr.jepc.util.QueryModify;

import java.util.Map;

/**
 * Binary greater relation.
 * 
 * @author Bastian Hoßbach
 * @author Marcus Pinnecke
 */
public class Greater extends Predicate {

	/** The left object of the greater relation */
	private Object left;
	
	/** The left object of the greater relation */
	private Object right;
	
	
	/**
	 * Creates a new greater relation.
	 * 
	 * @param left the left object of the greater relation
	 * @param right the right object of the greater relation
	 */
	public Greater(Object left, Object right) {
		this.left  = left;
		this.right = right;
	}

	
	/**
	 * Gets the left object of the greater relation.
	 * 
	 * @return the left object of the greater relation
	 */
	public Object getLeft() {
		return left;
	}


    /**
     * Sets the left object of the greater relation.
     *
     * @param left the left object of the greater relation
     */
    public void setLeft(Object left) {
        this.left = left;
    }

	
	/**
	 * Gets the right object of the greater relation.
	 * 
	 * @return the right object of the greater relation
	 */
	public Object getRight() {
		return right;
	}


    /**
     * Sets the right object of the greater relation.
     *
     * @param right the right object of the greater relation
     */
    public void setRight(Object right) {
        this.right = right;
    }

	
	@Override
	public String toString(){
		return left + " > " + right;
	}
	
	public Greater clone() {
		return new Greater(QueryModify.objectClone(left),QueryModify.objectClone(right));
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        return Evaluator.evaluate2(assignmentMap, left, right, (l, r) -> ((Comparable)l).compareTo(r) > 0);
    }
}