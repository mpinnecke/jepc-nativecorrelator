package de.umr.jepc.epa.parameter.predicate;

import java.util.Map;

/**
 * Interval relation.
 * 	             x		    
 *           |-------|
 * |------|   
 * @author Franz Ritter
 * @author Marcus Pinnecke
 *
 */
public class Before extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval Relation.  
	 * 
	 * @param symbol Symbol, which time interval is after the given pattern symbol.
	 */
	public Before(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the Before relation.
	 * 
	 * @return the symbol of the Before relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the Before relation.
	 * 
	 * @return the symbol of the Before relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the Before relation.
	 * 
	 * @return the symbol of the Before relation
	 */
	public char getSymbol() {
		return symbol;
	}
	
	
	@Override 
	public String toString(){
		return  "# |----| |----| "+symbol;
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException();
    }
}