package de.umr.jepc.epa.parameter.predicate;

import java.util.Map;

/**
 * Interval relation.
 * 	        x
 *     |-------|
 * |-------|
 *         
 * @author Franz Ritter
 * @author Marcus Pinnecke
 */
public class Overlap extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval relation.  
	 * 
	 * @param symbol Symbol, which time interval is after the overlapping time interval of the given pattern symbol
	 */
	public Overlap(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the Overlap relation.
	 * 
	 * @return the symbol of the Overlap relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the Overlap relation.
	 * 
	 * @return the symbol of Overlap Meet relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the OverlapWith relation.
	 * 
	 * @return the symbol of the OverlapWith relation
	 */
	public char getSymbol() {
		return symbol;
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}