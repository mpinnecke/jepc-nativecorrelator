package de.umr.jepc.epa.parameter.predicate;

import java.util.Map;

/**
 * Interval relation.
 * 	     
 *     |-----------|
 * |---------------|
 * 
 * @author Franz Ritter
 * @author Marcus Pinnecke
 *
 */
public class FinishedBy extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval relation.  
	 * 
	 * @param symbol Symbol, which time interval ends with the given pattern symbol
	 */
	public FinishedBy(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the FinishedBy relation.
	 * 
	 * @return the symbol of the FinishedBy relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the FinishedBy relation.
	 * 
	 * @return the symbol of FinishedBy Meet relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the FinishedBy relation.
	 * 
	 * @return the symbol of the FinishedBy relation
	 */
	public char getSymbol() {
		return symbol;
	}
	
	@Override 
	public String toString(){
		return  "finished by '"+symbol+"'";
	}
	
	public FinishedBy clone() {
		return new FinishedBy(new Character(symbol));
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}