package de.umr.jepc.epa.parameter.predicate;

import java.util.Map;

/**
 * Interval relation.
 * 	  x   
 *           |-------|
 * |------|   
 * @author Franz Ritter
 * @author Marcus Pinnecke
 *
 */
public class BeforeOf extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval Relation.  
	 * 
	 * @param symbol Symbol, which time interval is before the given pattern symbol.
	 */
	public BeforeOf(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the BeforeOf relation.
	 * 
	 * @return the symbol of the BeforeOf relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the BeforeOf relation.
	 * 
	 * @return the symbol of BeforeOf Meet relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the BeforeOf relation.
	 * 
	 * @return the symbol of the BeforeOf relation
	 */
	public char getSymbol() {
		return symbol;
	}
	
	@Override 
	public String toString(){
		return  "before of '"+symbol+"'";
	}
	
	public BeforeOf clone() {
		return new BeforeOf(new Character(symbol));
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException();
    }
}