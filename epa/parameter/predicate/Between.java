package de.umr.jepc.epa.parameter.predicate;

import de.umr.jepc.epa.parameter.booleanexpression.And;

public class Between extends And {
	
	public Between(String attribute, Object min, Object max) {
		super(new GreaterEqual(attribute,min),new LessEqual(attribute,max));
	}

}

