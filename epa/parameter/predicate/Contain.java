package de.umr.jepc.epa.parameter.predicate;

import java.util.Map;

/**
 * Interval relation.
 * 	       x
 *     |-------|
 * |---------------|
 *         
 * @author Franz Ritter
 * @author Marcus Pinnecke
 *
 */
public class Contain extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval relation.  
	 * 
	 * @param symbol Symbol, which time interval is contain of the interval of the given pattern Symbol
	 */
	public Contain(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the Contain relation.
	 * 
	 * @return the symbol of the Contain relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the Contain relation.
	 * 
	 * @return the symbol of Contain Meet relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the Contain relation.
	 * 
	 * @return the symbol of the Contain relation
	 */
	public char getSymbol() {
		return symbol;
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException();
    }
}