package de.umr.jepc.epa.parameter.predicate;

import java.util.Map;

/**
 * Interval relation.
 * 	        
 *     |-------|
 * |-------|
 *     x    
 * @author Franz Ritter
 * @author Marcus Pinnecke
 */
public class OverlapWith extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval relation.  
	 * 
	 * @param symbol Symbol, which time interval is before the overlapping time interval of the given pattern symbol
	 */
	public OverlapWith(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the OverlapWith relation.
	 * 
	 * @return the symbol of the OverlapWith relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the OverlapWith relation.
	 * 
	 * @return the symbol of OverlapWith Meet relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the OverlapWith relation.
	 * 
	 * @return the symbol of the OverlapWith relation
	 */
	public char getSymbol() {
		return symbol;
	}
	
	@Override 
	public String toString(){
		return  "overlap with '"+symbol+"'";
	}
	
	public OverlapWith clone() {
		return new OverlapWith(new Character(symbol));
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}