package de.umr.jepc.epa.parameter.predicate;

import de.umr.jepc.util.QueryModify;

import java.util.Map;

/**
 *  Interval relation.
 * 
 * 	  x
 * |------o------|
 * @author Franz Ritter
 * @author Marcus Pinnecke
 *
 */
public class MeetBy extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval Relation.  
	 * 
	 * @param symbol Symbol, which time interval is right before of the given pattern symbol.
	 */
	public MeetBy(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the MeetBy relation.
	 * 
	 * @return the symbol of the MeetBy relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the MeetBy relation.
	 * 
	 * @return the symbol of the MeetBy relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the MeetBy relation.
	 * 
	 * @return the symbol of the MeetBy relation
	 */
	public char getSymbol() {
		return symbol;
	}
	
	
	@Override 
	public String toString(){
		return  "meet by '"+symbol+"'";
	}
	
	public MeetBy clone() {
		return new MeetBy(new Character(symbol));
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}
