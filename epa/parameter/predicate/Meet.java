package de.umr.jepc.epa.parameter.predicate;

import java.util.Map;

/**
 *  Interval relation.
 * 
 * 	  		  x
 * |------o------|
 * @author Franz Ritter
 * @author Marcus Pinnecke
 *
 */
public class Meet extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval Relation.  
	 * 
	 * @param symbol Symbol, which time interval is immediately after the given pattern symbol.
	 */
	public Meet(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the Meet relation.
	 * 
	 * @return the symbol of the Meet relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the Meet relation.
	 * 
	 * @return the symbol of the Meet relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the Meet relation.
	 * 
	 * @return the symbol of the Meet relation
	 */
	public char getSymbol() {
		return symbol;
	}
	
	
	@Override 
	public String toString(){
		return  "# |----o----| "+symbol;
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
}