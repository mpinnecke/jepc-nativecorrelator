/*
 * Copyright (c) 2012, 2013
 * Database Research Group, University of Marburg.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.umr.jepc.epa.parameter.predicate;

import de.umr.jepc.engine.correlator.utils.Evaluator;
import de.umr.jepc.util.QueryModify;

import java.util.Map;

/**
 * Binary unequal relation.
 * 
 * @author Bastian Hoßbach
 * @author Marcus Pinnecke
 */
public class Equal extends Predicate {

	/** The left object of the equal relation */
	private Object left;
	
	/** The right object of the equal relation */
	private Object right;
	
	
	/**
	 * Creates a new equal relation.
	 * 
	 * @param left the left object of the equal relation
	 * @param right the right object of the equal relation
	 */
	public Equal(Object left, Object right) {
		this.left  = left;
		this.right = right;
	}

	
	/**
	 * Gets the left object of the equal relation.
	 * 
	 * @return the left object of the equal relation
	 */
	public Object getLeft() {
		return left;
	}

	
	/**
	 * Gets the right object of the equal relation.
	 * 
	 * @return the right object of the equal relation
	 */
	public Object getRight() {
		return right;
	}
	
	@Override 
	public String toString(){
		return left + " = "+ right;
	}
	
	public Equal clone() {
		return new Equal(QueryModify.objectClone(left),QueryModify.objectClone(right));
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        return Evaluator.evaluate2(assignmentMap, left, right, (l,r) -> l.equals(r));
    }
}