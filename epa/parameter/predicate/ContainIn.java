package de.umr.jepc.epa.parameter.predicate;

import java.util.Map;

/**
 * Interval relation.
 * 	     
 *     |-------|
 * |---------------|
 *         x
 * @author Franz Ritter
 * @author Marcus Pinnecke
 */
public class ContainIn extends Predicate{

	private char symbol;
	
	/**
	 * Creates a new interval relation.  
	 * 
	 * @param symbol Symbol, which time interval includes the given pattern symbol
	 */
	public ContainIn(char symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Gets the symbol of the ContainIn relation.
	 * 
	 * @return the symbol of the ContainIn relation
	 */
	public Object getLeft() {
		return symbol;
	}

	/**
	 * Gets the symbol of the ContainIn relation.
	 * 
	 * @return the symbol of ContainIn Meet relation
	 */
	public Object getRight() {
		return symbol;
	}
	
	/**
	 * Gets the symbol of the ContainIn relation.
	 * 
	 * @return the symbol of the ContainIn relation
	 */
	public char getSymbol() {
		return symbol;
	}
	
	@Override 
	public String toString(){
		return  "contain in '"+symbol+"'";
	}
	
	public ContainIn clone() {
		return new ContainIn(new Character(symbol));
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        throw new UnsupportedOperationException();
    }
}