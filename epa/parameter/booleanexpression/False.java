/*
 * Copyright (c) 2012, 2013
 * Database Research Group, University of Marburg.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.umr.jepc.epa.parameter.booleanexpression;

import java.util.Map;

/**
 * Constant Boolean expression that is always false.
 * 
 * @author Bastian Hoßbach
 * @author Marcus Pinnecke
 */
public class False extends Constant {

    /**
     * Creates a new constant Boolean expression that is always true.
     */
	public False() { }
	
	@Override
	public String toString(){
		return "false";
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        return false;
    }
}
