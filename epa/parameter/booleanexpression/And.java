/*
 * Copyright (c) 2012, 2013
 * Database Research Group, University of Marburg.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.umr.jepc.epa.parameter.booleanexpression;

import java.util.Map;

/**
 * Operator that combines Boolean expressions via
 * the logical 'and'.
 * 
 * @author Bastian Hoßbach
 * @author Marcus Pinnecke
 */
public class And extends Operator {

	/**
     * Left Boolean expression of the 'and' operator
     */
	private BooleanExpression left;
	
	/**
     * Right Boolean expression of the 'and' operator
     */
	private BooleanExpression right;
	
	
	/**
	 * Creates a new combination of two Boolean expression
     * on basis of the logical 'and'.
	 * 
	 * @param left left Boolean expression of the 'and' operator
	 * @param right right Boolean expression of the 'and' operator
	 */
	public And(BooleanExpression left, BooleanExpression right) {
		this.left  = left;
		this.right = right;
	}


    /**
     * Applies the 'and' operator to three or more boolean expressions.
     *
     * @param be1 the first boolean expression
     * @param be2 the second boolean expression
     * @param be3 the third boolean expression
     * @param be  arbitrary number of additional boolean expressions
     */
    public And(BooleanExpression be1, BooleanExpression be2, BooleanExpression be3, BooleanExpression ... be) {
        BooleanExpression[] all = new BooleanExpression[3 + be.length];
        all[0] = be1;
        all[1] = be2;
        all[2] = be3;
        if(be.length > 0)
            System.arraycopy(be, 0, all, 3,be.length);
        BooleanExpression rightBE = all[all.length-1];
        BooleanExpression temp;
        for(int i = all.length-2; i > 0; i--) {
            temp    = new And(all[i], rightBE);
            rightBE = temp;
        }
        this.left  = all[0];
        this.right = rightBE;
    }

	
	/**
	 * Gets the left boolean expression of the 'and' operator.
	 * 
	 * @return the left boolean expression of the 'and' operator
	 */
	public BooleanExpression getLeft() {
		return left;
	}

	
	/**
	 * Gets the right boolean expression of the 'and' operator.
	 * 
	 * @return the right boolean expression of the 'and' operator
	 */
	public BooleanExpression getRight() {
		return right;
	}
	
	@Override
	public String toString(){
		return left + " and " + right;
	}

    @Override
    public boolean eval(Map<String, Object> assignmentMap) {
        return left.eval(assignmentMap) && right.eval(assignmentMap);
    }
}
