# JEPC NativeCorrelator #

This repository contains the project code for adding support of binary stream joins over data streams in Java Event Processing Connectivity (JEPC) by Database Research Group, University of Marburg (see [Project Homepage](http://dbs.mathematik.uni-marburg.de/research/projects/jepc/index.html)). The binary stream operator is entirely written in Java and based of [Continuous Queries over Data Streams - Semantics and Implementation](http://archiv.ub.uni-marburg.de/diss/z2007/0671/) by Jürgen Krämer. Please note that this repository only contains the code for the operator as well as necessary changes in the core project code. This is **not** to full JEPC repository or code base. To get the source [visit the download section of the project homepage](http://dbs.mathematik.uni-marburg.de/research/projects/jepc/download.html).

## Setup and Requirements ##

To get this code running you have to

* Get your own copy of the JEPC source code [here](http://dbs.mathematik.uni-marburg.de/research/projects/jepc/download.html)

* Integrate the code inside this repository to your copy

* Ensure the project is set to **Java 1.8** language level


*Note*: This project will be merged into the JEPC repository as soon as possible.